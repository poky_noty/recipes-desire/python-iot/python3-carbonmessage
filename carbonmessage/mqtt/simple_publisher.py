import paho.mqtt.publish as publish


class SimplePublisher(object):
    def __init__(self, logger, broker):
        self.logger = logger

        self.topics = broker.topics
        self.broker = broker

    def publish_single_message_to_topic(self, tindex=0, payload="Hello IoT"):
        my_topic = self.topics[tindex]

        self.logger.debug(
            "Ready to Publish Single MQTT message: %s/ %s/ %s:%d", my_topic,
            payload, self.broker.host, self.broker.port)

        publish.single(topic=my_topic,
                       payload=payload,
                       hostname=self.broker.host,
                       port=self.broker.port)

    def publish_many_messages(self):
        self.logger.debug("Ready to Publish Multiple MQTT messages")
        msgs = [{
            'topic': self.topics[1],
            'payload': "multiple 1"
        }, (self.topics[2], "multiple 2", 0, False)]
        publish.multiple(msgs, hostname="test.mosquitto.org")
