import paho.mqtt.subscribe as subscribe


class SimpleSubscriber(object):
    def __init__(self, logger, broker):
        self.logger = logger
        self.topics = broker.topics
        self.broker = broker

    def subscribe_and_wait_messages(self, msg_count=1):
        all_msgs = subscribe.simple(
            self.topics, hostname=self.broker.host, port=self.broker.port,
            retained=False, msg_count=msg_count)

        for msg in all_msgs:
            self.logger.debug(msg.topic)
            self.logger.debug(msg.payload)

    def subscribe_async(self):
        pass
