from .mqtt.simple_subscriber import SimpleSubscriber
from .mqtt.simple_publisher import SimplePublisher


class MQTTMessagesRadar(object):
    def __init__(self, logger, broker):
        self.logger = logger
        self.broker = broker

    def subscribe_to_topics(self):
        simple = SimpleSubscriber(self.logger, self.broker)
        simple.subscribe_and_wait_messages()

    def publish_to_topics(self):
        simple = SimplePublisher(self.logger, self.broker)
        simple.publish_single_message_to_topic()
        #simple.publish_multiple_messages_to_topic()
