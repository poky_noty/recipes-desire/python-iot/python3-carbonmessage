#from .utils.external_configuration import read_config, get_section
from .utils.external_configuration import ExternalConfiguration
#from .utils.local_logger import LocalLogger

class PlatformConstruction(object):
    _logger = None

    def __init__(self, conf_filename):
        self.conf_filename = conf_filename

    def apply_external_configuration(self):
        #print( "Ready to Read External Configuration ..." )

        #read_config(self.conf_filename)
        ext_config = ExternalConfiguration(self.conf_filename)
        ext_config.read_config()

        #self._default_section = get_section("default")

        #self._local_broker = get_section("local-broker")
        #self._central_broker = get_section("central-broker")

        #print( "Default Section Data is ...{}".format( self.default_section.pid ) )

    def prepare_logger(self):
        #local_logger = LocalLogger()
        #local_logger.setup(filename=self.default_section.log_filename,
        #          level=self.default_section.log_level,
        #          msg_format=self.default_section.msg_format,
        #          maxBytes=self.default_section.max_bytes,
        #          backupCount=self.default_section.backup_count)

        #self._logger = get_logger()
        pass

    def turn_key_solution(self):
        """
        Before starting message-forwarder have to setup logger and setup all
        external-configuration parameters.
        Only after these tasks the two proxies can establish a communication
        layer for performance data.
        """
        #print( "Ready to Start MQTT Platform Construction !" )
        self.apply_external_configuration()
        self.prepare_logger()

    def debug(self, message):
        self._logger.debug(message)

    def error(self, message):
        self._logger.error(message)

    def info(self, message):
        self._logger.info(message)

    @property
    def local_broker(self):
        pass
        #return self._local_broker

    @property
    def central_broker(self):
        pass
        #return self._central_broker

    @property
    def default_section(self):
        pass
        #return self._default_section

    @property
    def logger(self):
        return self._logger
