from .platform_construction import PlatformConstruction
#from .mqtt_messages_radar import MQTTMessagesRadar


def main():
    # Read external configuration & setup various supporting modules
    platform = PlatformConstruction(
        conf_filename="/etc/carbonmessage/carbonmessage.conf")

    platform.turn_key_solution()

    msg = ("\n********************************"+
           "\nMQTT Platform is Ready for War :"+
           "\n********************************\n"+
           "\nIs-Daemon ... {}".format(platform.default_section.is_daemon)+
           "\nLocal-Broker ... {}:{}".format(
               platform.local_broker.host, platform.local_broker.port)+
           "\nCentral-Broker ... {}:{}".format(
               platform.central_broker.host, platform.central_broker.port))

    platform.debug(msg)

    #radar = MQTTMessagesRadar(platform.logger, platform.local_broker)

# Application entrypoint to start server prodedure ... Scan all mqtt-brokers for new messages
#def main_subscribe():
#    radar.subscribe_to_topics()


#def main_publish():
#    radar.publish_to_topics()
