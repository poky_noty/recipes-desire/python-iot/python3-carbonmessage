class BrokerData(object):
    _is_local = True
    _is_available = True

    """
    Necessary data to establish communication with an MQTT-Broker for IoT messages.
    """
    def __init__(self,
                 host,
                 port,
                 topics):
        self._host = host
        self._port = port

        self._topics = topics

    @property
    def is_local(self):
        return self._is_local

    @property
    def host(self):
        return self._host

    @property
    def port(self):
        return self._port

    @property
    def is_available(self):
        return self._is_available

    @property
    def topics(self):
        return self._topics

    def set_is_local(self, is_local):
        self._is_local = is_local

    def set_is_available(self, is_available):
        self._is_available = is_available
