class DefaultSectionData(object):
    """
    This class contains all configuration data defined in default section of .CONF
    """
    def __init__(self,
                 pid,
                 msg_format,
                 is_daemon=False,
                 log_filename="/var/log/carbonmessage/carbonmessage.conf",
                 log_level="DEBUG",
                 max_bytes=100,
                 backup_count=5):
        self._is_daemon = is_daemon
        self._pid = pid

        self._log_filename = log_filename
        self._log_level = log_level
        self._msg_format = msg_format

        self._max_bytes = max_bytes
        self._backup_count = backup_count

    @property
    def is_daemon(self):
        return self._is_daemon

    @property
    def pid(self):
        return self._pid

    @property
    def log_filename(self):
        return self._log_filename

    @property
    def log_level(self):
        return self._log_level

    @property
    def msg_format(self):
        return self._msg_format

    @property
    def max_bytes(self):
        return self._max_bytes

    @property
    def backup_count(self):
        return self._backup_count
