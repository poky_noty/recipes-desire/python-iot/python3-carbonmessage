import os.path

from configparser import ConfigParser, ExtendedInterpolation

from .default_section_data import DefaultSectionData
from .broker_data import BrokerData

class ExternalConfigurationException(Exception):
    def __init__(self, message, errors=None):
        super().__init__(message)

        self.errors = errors

class ExternalConfiguration(object):
    default_section = None

    logger_data = None
    local_broker_data = None
    remote_broker_data = None

    def __init__(self, filename_conf):
        self.filename_conf = filename_conf

    def read_config(self):
        """
        Read external configuration file and assign data to local variables.
        """
        if not self.filename_conf:
            raise TypeError("Cannot Proceed with External-Configuration ... "+
                            "Undefined Filename !")
        elif not os.path.isfile(self.filename_conf):
            raise FileNotFoundError("Cannot Proceed with External-Configuration ... "+
                                    "File Not Found '{}'".format(self.filename_conf))

        config = ConfigParser(interpolation=ExtendedInterpolation())
        config_data = config.read(self.filename_conf)
        if not config_data:
            # Nothing read .. aborting
            raise ExternalConfigurationException("Problem Reading External CONF file ... "+
                                                 "No Data Returned from {}!".format(
                                                     self.filename_conf))
        # Is-Daemon ?
        is_daemon = config.getboolean("default", "is-daemon")
        pid = config.get("default", "pid")

        # Logging
        log_filename = config.get("default", "log-filename")
        log_level = config.get("default", "log-level")
        msg_format = config.get("default", "msg-format")
        max_bytes = config.getint("default", "max-bytes")
        backup_count = config.getint("default", "backup-count")

        self.default_section = DefaultSectionData(
            is_daemon=is_daemon, pid=pid, log_filename=log_filename,
            log_level=log_level, msg_format=msg_format, max_bytes=max_bytes,
            backup_count=backup_count)

        # Broker - Local
        """is_available = config.getboolean("local-broker", "is-available")

        host = config.get("local-broker", "host")
        port = config.getint("local-broker", "port")

        topics = config.get("local-broker", "topics")
        _topics = topics.split(",")
        t_opics = []

        #print("Topics array is ... {}".format(_topics))
        for topic in _topics:
            # Topics without trailing and leading spaces
            t_opics.append(topic.strip())
            #print("Topics Array is .... {}".format(t_opics))

            self.local_broker_data = BrokerData(
                is_local=True, is_available=is_available, port=port,
                host=host, topics=t_opics)

            # Broker - Central
            _is_available = config.getboolean("central-broker", "is-available")

            _host = config.get("central-broker", "host")
            _port = config.getint("central-broker", "port")

            self.central_broker_data = BrokerData(
                is_local=False, is_available=_is_available, port=_port, host=_host)
        """

    def get_section(self, name):
        if name.lower() == "default":
            # [default]
            return self.default_section
        elif name.lower() == "local-broker":
            return self.local_broker_data
        elif name.lower() == "central-broker":
            return self.remote_broker_data
        else:
            # Unknown section
            raise Exception("Section Unknown")
