import logging
from logging.handlers import RotatingFileHandler

LEVELS = {
    "CRITICAL": logging.CRITICAL,
    "ERROR": logging.ERROR,
    "WARNING": logging.WARNING,
    "INFO": logging.INFO,
    "DEBUG": logging.DEBUG,
}

class LocalLogger(object):
    """
    This is useful logger for sending messages into a local file.
    It is using a recursive scheme that opens a new file and archieves the
    old one in case of a size-limit is exceeded.
    """
    logger = None

    def __init__(self, logger_data):
        """
        Define the important parameters for a file logger.
        """
        self.logger_data = logger_data

    def setup(self):
        """
        Setup a common logger on a module level
        """
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(LEVELS[self.logger_data.level])

        # sudo chmod 777 /var/log/nmapwrapper/nmapwrapper.log
        file_handler = RotatingFileHandler(filename=self.logger_data.filename,
                                           maxBytes=self.logger_data.max_bytes * 1024 * 1024,
                                           backupCount=self.logger_data.backup_count)

        formatter = logging.Formatter(self.logger_data.msg_format)
        file_handler.setFormatter(formatter)

        self.logger.addHandler(file_handler)
