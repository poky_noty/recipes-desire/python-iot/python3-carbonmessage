class LoggerData(object):
    _filename = None
    _level = None
    _max_bytes = None
    _backup_count = None
    _msg_format = None

    def __init__(self, filename, max_bytes, backup_count):
        self._filename = filename
        self._max_bytes = max_bytes
        self._backup_count = backup_count

    def set_level(self, level):
        self._level = level

    def set_msg_format(self, msg_format):
        self._msg_format = msg_format

    @property
    def filename(self):
        return self._filename
