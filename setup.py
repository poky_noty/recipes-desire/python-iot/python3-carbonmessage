from setuptools import setup

#
###
setup(
    name="carbonmessage",
    version="1.0.0",
    description=
    "Python3 module scanning new messages in mqtt-brokers and sending emails in case of important events",
    keywords="mqtt-broker iot messages abnomaly-detection notifications",
    # Project's main page
    url="https://gitlab.com/recipes-desire/python-iot/python3-carbonmessage",
    # Author details
    author="Miltos K. Vimplis",
    author_email="mvimblis@gmail.com",
    # Choose your license
    license="MIT License",
    classifiers=[
        "Programing Language :: Python :: 3.7", "Operating System :: Linux",
        "Environment :: Console"
    ],
    packages=[
        "carbonmessage", "carbonmessage.anomalies", "carbonmessage.mqtt",
        "carbonmessage.utils"
    ],
    #install_requires=[""],
    entry_points={
        "console_scripts": [
            "carbonmessage=carbonmessage.server:main",
        ],
    },
    zip_safe=True)
